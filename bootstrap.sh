#/bin/bash

if [[ "$OSTYPE" == "linux-gnu" ]]; then

	# Install ansible on the maschine
	sudo apt-get update
	sudo apt-get -y install software-properties-common
	sudo apt-add-repository -y ppa:ansible/ansible
	sudo apt-get update
	sudo apt-get -y install ansible

	# add entry in ansible hosts file
	sudo chown $(whoami):root /etc/ansible/hosts
	sudo echo -e "[local]
	localhost ansible_connection=local ansible_user=$(whoami)" >> /etc/ansible/hosts
	sudo chown root:root /etc/ansible/hosts

elif [[ "$OSTYPE" == "darwin"* ]]; then

	# Install ansible
#	pip3 install --user ansible

 	# add entry in ansible hosts file
	sudo mkdir -p /etc/ansible/
	sudo touch /etc/ansible/hosts
        sudo chown $(whoami):wheel /etc/ansible/hosts
        sudo echo  "[local]
localhost ansible_connection=local ansible_user=$(whoami)" > /etc/ansible/hosts
        sudo chown root:wheel /etc/ansible/hosts

else
	echo "There is no support at the moment for $OSTYPE OS..."
fi

